#include <iostream>

using namespace std;

template <typename T>
class Node{
public:
    T data;
    Node* next;
    Node(T _data) : data(_data) {}
};

template <typename T>
class List{
public:
    Node<T>* head = nullptr;
    bool upis(T data){
        auto* node = new Node<T>(data);
        Node<T>** p = &head;
        for(; *p && (*p)->data < data; p = &(*p)->next);
        Node<T>* tmp = *p;
        *p = node;
        node->next = tmp;
        return true;
    }
    void ispis(){
        Node<T>** p = &head;
        for(; *p;p = &(*p)->next){
            cout << "  " << (*p)->data;
        }
    }
};

int main() {

    int entry;

    auto* list = new List<int>;

    for (int i = 0; i < 10; i++) {
        cin >> entry;
        list->upis(entry);
    }

    list->ispis();

    return 0;
}