//
// Created by nikolak on 08. 12. 2021..
//

#include <iostream>

using namespace std;

class Stack{
private:
    int stack[10];
    int top = -1;
public:
    bool push(int n){
        if(top > 9){
            return false;
        }
        top++;
        stack[top] = n;
        return true;
    }
    bool pop(int* n){
        if(top < 0){
            return false;
        }
        *n = stack[top];
        top--;
        return true;
    }
};

int main(){

    auto* stack = new Stack;
    auto* elp = new Stack;
    int num;

    for (int i = 0; i < 10; i++) {
        stack->push(rand() % 10 + 1);
    }


    for (int i = 0; i < 10; i++) {
        stack->pop(&num);
        elp->push(num);
        cout << "  " << num;
    }

    cout << endl;

    for (int i = 0; i < 10; i++) {
        elp->pop(&num);
        stack->push(num);
        cout << "  " << num;
    }

    return 0;
}