//
// Created by nikolak on 09. 12. 2021..
//

#include <iostream>

using namespace std;

static inline void Zamena(string *left, string *right) {
    string temp = *left;
    *left = *right;
    *right = temp;
}

void BubbleSortPoboljsani(string A[], int N, int smjer){
    int i, j;
    bool swapHappened = true;
    for (i = 0; swapHappened; i++) {
        swapHappened = false;
        for (j = 0; j < N - 1 - i; j++) {
            if (!smjer && A[j + 1] < A[j] || smjer && A[j + 1] > A[j]) {
                Zamena(&A[j], &A[j + 1]);
                swapHappened = true;
            }
        }
    }
}

int main(){

    string arr[] =  {"Ivo", "Marko", "Juraj", "Pero"};

    for (auto & i : arr) {
        cout << i << "  ";
    }

    BubbleSortPoboljsani(arr, 4, 1);


    cout << endl;
    for (auto & i : arr) {
        cout << i << "  ";
    }

    return 0;
}