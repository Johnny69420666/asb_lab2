//
// Created by nikolak on 10. 12. 2021..
//

#include <iostream>

using namespace std;

class Node{
public:
    double data;
    Node* next;
    Node(double _data) : data(_data) {}
};

class Queue{
private:
    Node* read;
    Node* write;
public:
    bool enqueue(double data){
        auto* node = new Node(data);
        if(write == nullptr){
            read = node;
            write = node;
            return true;
        }
        write->next = node;
        write = node;
        return true;
    }
    bool dequeue(double &data){
        if(read == nullptr){
            return false;
        }
        data = read->data;
        auto* tmp = read;
        read = read->next;
        delete tmp;
        return true;
    }
};



int main() {

    auto* q = new Queue;
    double x;

    q->enqueue(1.23);
    q->enqueue(1.234);
    q->enqueue(1.2345);
    q->enqueue(1.23456);
    q->enqueue(1.234567);
    q->enqueue(1.2345689);
    q->enqueue(69);
    q->dequeue(x);
    q->dequeue(x);
    q->dequeue(x);
    q->dequeue(x);

    cout << x;

    return 0;
}