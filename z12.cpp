//
// Created by nikolak on 09. 12. 2021..
//

#include <iostream>

using namespace std;

template <class T>
void InsertionSort(T A[], int N){
    int i, j;
    T tmp;
    for (i = 1; i < N; ++i) {
        tmp = A[i];
        for (j = i; j >= 1 && A[j-1] > tmp ; j--) {
            A[j] = A[j-1];
        }
        A[j] = tmp;
    }
}


class Osoba{
private:
    string name;
    short unsigned int age;
public:
    //Constructors
    Osoba(string _name, short unsigned int _age) : name(_name), age(_age) {}
    Osoba(){}

    //Operators
    friend bool operator<(const Osoba& o1, const Osoba& o2);
    friend bool operator>(const Osoba& o1, const Osoba& o2);
    friend ostream& operator<<(ostream& os, const Osoba& o);

};

bool operator<(const Osoba &o1, const Osoba &o2) {
    if(o1.age < o2.age){
        return true;
    }
    if(o1.age == o2.age && o1.name.substr(0,1) < o2.name.substr(0,1)){
        return true;
    }
    return false;
}

bool operator>(const Osoba &o1, const Osoba &o2) {
    if(o1.age > o2.age){
        return true;
    }
    if(o1.age == o2.age && o1.name.substr(0,1) > o1.name.substr(0,1)){
        return true;
    }
    return false;
}

ostream &operator<<(ostream& os, const Osoba &o) {
    os << "{" << o.name << ", " << o.age << "}";
    return os;
}

template <typename T> static void printArray(T A[], size_t n) {
    for (auto i = 0; i < n; i++) {
        cout << A[i] << " ";
    }
    cout << endl;
}

int main(){

    Osoba arr[] = {
            Osoba("Ana", 20),
            Osoba("Ivo", 9),
            Osoba("Marko", 9),
            Osoba("Lidija", 22),
            Osoba("Pero", 19)
    };

    printArray(arr, 5);
    InsertionSort(arr, 5);
    printArray(arr, 5);

    return 0;
}