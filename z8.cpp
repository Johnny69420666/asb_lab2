//
// Created by nikolak on 10. 12. 2021..
//

#import<iostream>

using namespace std;

class Node{
public:
    int data;
    Node* next;
    Node(int _data) : data(_data) {};
};

class Queue{
private:
    Node* read;
    Node* write;
public:
    bool enqueue(int data){
        auto* node = new Node(data);
        if(write == nullptr){
            read = node;
            write = node;
            return true;
        }
        write->next = node;
        write = node;
        return true;
    }
    bool enqueueArr(int arr[], int n){
        if(n < 0) return true;
        bool added = enqueue(arr[n]);
        if(!added) return false;
        enqueueArr(arr, n-1);
    }

    void print(){
        Node** p = &read;
        cout << "<<";
        for(; *p; p = &(*p)->next){
            cout << "  " << (*p)->data;
        }
        cout << "  <<" << endl;
    }
};

int main(){

    int arr[10];
    for (int i = 0; i < 10; ++i) {
        arr[i] = i;
    }

    auto* Q = new Queue();
    Q->enqueueArr(arr, 9);

    Q->print();

    return 0;
}