//
// Created by nikolak on 08. 12. 2021..
//

#include <iostream>

using namespace std;

template <typename T>
class Node{
public:
    T data;
    Node* next;
    Node(T _data) : data(_data) {}
};

template <typename T>
class List{
public:
    Node<T>* head = nullptr;
    bool upis(T element){
        auto* node = new Node<T>(element);
        Node<T>** p = &head;
        for(; *p; p = &(*p)->next);
        *p = node;
        return true;
    }
    void ispis(){
        Node<T>** p = &head;
        for(; *p; p = &(*p)->next){
            cout << "  " << (*p)->data;
        }
    }
};

int main(){

    string string1;
    auto* list = new List<string>;

    for (int i = 0; i < 10; i++) {
        cin >> string1;
        list->upis(string1);
    }

    list->ispis();

    return 0;
}