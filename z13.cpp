//
// Created by nikolak on 09. 12. 2021..
//

//
// Created by nikolak on 09. 12. 2021..
//

#include <iostream>

using namespace std;

template <typename T> static inline void Swap(T *left, T *right) {
    T temp = *left;
    *left = *right;
    *right = temp;
}

template <class T>
static void SelectionSort(T A[], size_t n) {
    size_t i, j, min;
    for (i = 0; i < n; i++) {
        min = i;
        for (j = i + 1; j < n; j++) {
            if (A[j] < A[min])
                min = j;
        }
        Swap(&A[i], &A[min]);
    }
}


class Vozilo{
private:
    string name;
    int age;
public:
    //Constructors
    Vozilo(string _name, int _age) : name(_name), age(_age) {}
    Vozilo(){}

    //Operators
    friend bool operator<(const Vozilo& o1, const Vozilo& o2);
    friend bool operator>(const Vozilo& o1, const Vozilo& o2);
    friend ostream& operator<<(ostream& os, const Vozilo& o);

};

bool operator<(const Vozilo &o1, const Vozilo &o2) {
    if(o1.name < o2.name){
        return true;
    }
    if(o1.name == o2.name && o1.age > o2.age){
        return true;
    }
    return false;
}

bool operator>(const Vozilo &o1, const Vozilo &o2) {
    if(o1.name > o2.name){
        return true;
    }
    if(o1.name == o2.name && o1.age < o2.age){
        return true;
    }
    return false;
}

ostream &operator<<(ostream& os, const Vozilo &o) {
    os << "{" << o.name << ", " << o.age << "}";
    return os;
}

template <typename T> static void printArray(T A[], size_t n) {
    for (auto i = 0; i < n; i++) {
        cout << A[i] << " ";
    }
    cout << endl;
}

int main(){

    Vozilo arr[] = {
        {"Pauegot",1981},
        {"Pauegot",1983},
        {"Ranulet",1967},
        {"Fait",1972},
        {"BWM",1985},
        {"Merdesec",1983}
    };

    printArray(arr, 6);
    SelectionSort(arr, 6);
    printArray(arr, 6);

    return 0;
}