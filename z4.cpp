//
// Created by nikolak on 08. 12. 2021..
//

#include <iostream>

using namespace std;

class Stack{
private:
    int* stack = (int*)malloc(100*sizeof(int));
    int* top = stack;
public:
    bool push(int n){
        if(top >= stack + 99*sizeof(int)){
            return false;
        }
        *top = n;
        top++;
        return true;
    }
    void print(){
        for (int i = 0; i < 100; i++) {
            cout << "  " << i << ": " << stack[i] << endl;
        }
    }
};

int main(){

    auto* stack = new Stack;

    for (int i = 0; i < 102; i++) {
        stack->push(rand() % 100 + 1);
    }

    stack->print();

    return 0;
}