//
// Created by nikolak on 08. 12. 2021..
//

#include <iostream>

using namespace std;

class Queue{
private:
    int read = 0, write = 0;
    double queue[10];
public:
    bool enqueue(double x){
        if ((write + 1) % 10 == read)
            return false;
        queue[write] = x;
        write = (write + 1) % 10;
        return true;
    }
    bool dequeue(double &item){
        if (write == read)
            return false;
        item = queue[read];
        read = (read + 1) % 10;
        return true;
    }
    void print(){
        if(read == write){return;}
        for (int i = read; i < 10; i++) {
            cout << "  " << queue[i%10];
        }
    }
};

int main(){

    auto* q = new Queue;
    double x;

    for (int i = 0; i < 10; i++) {
        cin >> x;
        q->enqueue(x);
    }

    q->print();

    for (int i = 0; i < 10; i++) {
        q->dequeue(x);
    }

    q->print();

    return 0;
}